import * as vscode from 'vscode'

export function activate(context: vscode.ExtensionContext) {
  vscode.languages.registerDocumentFormattingEditProvider('apacheconf', {
    provideDocumentFormattingEdits(document: vscode.TextDocument): vscode.TextEdit[] {
      const edits: vscode.TextEdit[] = []
      const spaces = 4
      let indent = 0
      let lineBeforeIsEmptyOrWhitespace = false
      let lineBeforeIsTagOpen = false

      for (let i = 0; i < document.lineCount; i++) {
        const line = document.lineAt(i)

        // check if empty or whitespace => delete consecutive empty lines and after tag opens
        if (line.isEmptyOrWhitespace) {
          if (lineBeforeIsEmptyOrWhitespace) {
            // pop because last line clear would result in an exeption
            edits.pop()
            edits.push(vscode.TextEdit.delete(document.lineAt(i - 1).rangeIncludingLineBreak))
          }
          if (lineBeforeIsTagOpen) {
            edits.push(vscode.TextEdit.delete(new vscode.Range(document.lineAt(i - 1).range.end, line.range.start)))
          }
          edits.push(vscode.TextEdit.delete(line.range))
          lineBeforeIsEmptyOrWhitespace = true
          lineBeforeIsTagOpen = false
          continue
        } else {
          lineBeforeIsEmptyOrWhitespace = false
        }

        // trim whitespaces from end of line
        let lastNonWhitespaceCharPosition: vscode.Position = new vscode.Position(line.lineNumber, 0)
        for (let i = 0; i < line.text.length; i++) {
          lastNonWhitespaceCharPosition = /\s/.test(line.text.charAt(i))
            ? lastNonWhitespaceCharPosition
            : new vscode.Position(line.lineNumber, i)
        }
        if (!line.range.end.isEqual(lastNonWhitespaceCharPosition.translate(0, 1))) {
          edits.push(
            vscode.TextEdit.delete(new vscode.Range(lastNonWhitespaceCharPosition.translate(0, 1), line.range.end)),
          )
        }

        // decrease indent if on tag close
        if (
          line.text.charAt(line.firstNonWhitespaceCharacterIndex) === '<' &&
          line.text.charAt(line.firstNonWhitespaceCharacterIndex + 1) === '/'
        ) {
          indent -= 1
        }

        // delete indent
        edits.push(
          vscode.TextEdit.delete(
            new vscode.Range(
              line.range.start,
              new vscode.Position(line.lineNumber, line.firstNonWhitespaceCharacterIndex),
            ),
          ),
        )

        // insert indent
        if (indent !== 0) {
          edits.push(vscode.TextEdit.insert(line.range.start, ' '.repeat(spaces).repeat(indent)))
        }

        // increase indent afterwards if on tag open and set bool
        if (
          line.text.charAt(line.firstNonWhitespaceCharacterIndex) === '<' &&
          line.text.charAt(line.firstNonWhitespaceCharacterIndex + 1) !== '/'
        ) {
          indent += 1
          lineBeforeIsTagOpen = true
        } else {
          lineBeforeIsTagOpen = false
        }
      }

      // add empty line to end if it ends on characters
      const lastLine = document.lineAt(document.lineCount - 1)
      if (!lastLine.isEmptyOrWhitespace) {
        edits.push(vscode.TextEdit.insert(lastLine.range.end, '\n'))
      }
      return edits
    },
  })
}

export function deactivate() {}
