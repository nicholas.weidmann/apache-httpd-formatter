# apache-httpd-formatter

Formatter for Apache HTTP Server Project (httpd) configuration files.

## Features

Formats .conf files.

## Requirements

Depends on the Apache Conf Extension to detect the language.

## Extension Settings

None.

## Known Issues

None.

## Release Notes

None.
